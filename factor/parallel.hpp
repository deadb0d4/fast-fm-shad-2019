#pragma once

#include <cmath>
#include <atomic>

#include <factor/lock_vector.hpp>
#include <deps/Eigen/Dense>


namespace factor {

struct ThreadSafeModel {
  inline ThreadSafeModel(size_t feature_count, size_t dimension) :
      dim_(dimension),
      bias_(0),
      weight_(feature_count),
      vector_(feature_count) {
    for (auto&& vec : vector_) {
      vec.Set(Eigen::VectorXd::Random(dim_) / std::sqrt(static_cast<double>(dim_)));
    }
    for (auto&& wgt : weight_) {
      wgt.store(0);
    }
  }

  template <typename Feature>
  inline double Predict(const Feature& feature) const {
    double predict = bias_.load();

    Eigen::VectorXd sum = Eigen::VectorXd::Zero(dim_);
    for (auto&& [idx, val] : feature) {
      predict += weight_[idx].load() * val;
      sum += val * vector_[idx].Get();
    }
    predict += 0.5 * sum.dot(sum);
    return predict;
  }

  template <typename Feature, typename Optimizer, typename Loss>
  inline void Fit(
      const Feature& feature,
      double target,
      const Loss& loss,
      const Optimizer& opt) {
    // Forward step
    double predict = bias_;

    Eigen::VectorXd sum = Eigen::VectorXd::Zero(dim_);
    for (auto&& [idx, val] : feature) {
      predict += weight_[idx].load() * val;
      sum += val * vector_[idx].Get();
    }
    predict += 0.5 * sum.dot(sum);

    // Backward step
    double loss_gradient = loss.GetGradient(predict, target);
    for (auto&& [idx, val] : feature) {
      weight_[idx].store(opt.Step(weight_[idx].load(), val * loss_gradient));
      vector_[idx].Set(
          opt.template Step<Eigen::VectorXd>(
            vector_[idx].Get(),
            val * loss_gradient * sum
        )
      );
    }
    bias_.store(opt.Step(bias_.load(), loss_gradient));
  }


  const size_t dim_;

  std::atomic<double> bias_;
  std::vector<std::atomic<double>> weight_;
  std::vector<LockVector> vector_;
};

} // namespace factor
