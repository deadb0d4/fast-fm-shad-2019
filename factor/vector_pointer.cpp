#include <factor/vector_pointer.hpp>

namespace factor {

VectorPointer::VectorPointer() :
    lock(),
    pointer(nullptr) {}

VectorPointer::~VectorPointer() {
  delete pointer.exchange(nullptr);
}

void VectorPointer::Set(const Eigen::VectorXd& value) {
  auto old_pointer = pointer.exchange(new Eigen::VectorXd(value));

  lock.synchronize();
  delete old_pointer;
}

Eigen::VectorXd VectorPointer::Get() const {
  std::shared_lock guard(lock);

  return *(pointer.load());
}

} // namespace factor
