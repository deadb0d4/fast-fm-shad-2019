#pragma once


#include <atomic>
#include <pthread.h>


namespace factor {

template <typename T>
class ThreadLocal {
 private:
  pthread_key_t key_;

  struct Node {
    T value;
    Node* next;

    Node(Node* head = nullptr) : value(), next(head) {}
  };
  std::atomic<Node*> head_;

 public:
  ThreadLocal() : key_(), head_(nullptr) {
    pthread_key_create(&key_, nullptr);
  }

  ~ThreadLocal() {
    auto head = head_.load();
    while (head != nullptr) {
      auto prev = head;
      head = head->next;
      delete prev;
    }
    pthread_key_delete(key_);
  }

  T& operator*() {
    auto cell = static_cast<Node*>(pthread_getspecific(key_));
    if (cell == nullptr) {
      auto new_node = new Node();

      auto old_head = head_.load();
      do {
        new_node->next = old_head;
      } while (!head_.compare_exchange_strong(old_head, new_node));
      pthread_setspecific(key_, new_node);

      return new_node->value;
    }
    return cell->value;
  }

  T* operator->() {
    auto cell = static_cast<Node*>(pthread_getspecific(key_));
    if (cell == nullptr) {
      auto new_node = new Node();

      auto old_head = head_.load();
      do {
        new_node->next = old_head;
      } while (!head_.compare_exchange_strong(old_head, new_node));
      pthread_setspecific(key_, new_node);

      return &(new_node->value);
    }
    return &(cell->value);
  }

  class Iterator {
   public:
    Iterator(Node* ptr = nullptr) : ptr_(ptr) {
    }

    Iterator(const Iterator& other) : ptr_(other.ptr_) {
    }

    void operator++() {
      ptr_ = ptr_->next;
    }

    void operator++(int) {
      ptr_ = ptr_->next;
    }

    bool operator==(const Iterator& other) const {
      return ptr_ == other.ptr_;
    }

    bool operator!=(const Iterator& other) const {
      return ptr_ != other.ptr_;
    }

    T& operator*() const {
      return ptr_->value;
    }

    T* operator->() const {
      return &(ptr_->value);
    }

   private:
    Node* ptr_;
  };

  Iterator begin() {  // NOLINT
    return Iterator(head_.load());
  }

  Iterator end() {  // NOLINT
    return Iterator();
  }
};

} // namespace factor
