#pragma once

#include <mutex>
#include <deps/Eigen/Dense>


namespace factor {

struct LockVector {
  mutable std::mutex lock;
  Eigen::VectorXd value;

  void Set(const Eigen::VectorXd& other);

  Eigen::VectorXd Get() const;
};

}
