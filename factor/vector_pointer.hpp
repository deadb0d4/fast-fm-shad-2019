#pragma once

#include <deps/Eigen/Dense>
#include <atomic>
#include <shared_mutex>
#include <factor/rcu.hpp>


struct VectorPointer {
  mutable factor::rcu_lock lock;
  std::atomic<Eigen::VectorXd*> pointer;

  VectorPointer();

  ~VectorPointer();

  void Set(const Eigen::VectorXd& value);

  Eigen::VectorXd Get() const;
};
