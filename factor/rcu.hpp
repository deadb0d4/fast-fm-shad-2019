#pragma once

#include <shared_mutex>
#include <atomic>
#include <vector>
#include <type_traits>

#include <factor/thread_local.hpp>


namespace factor {

class rcu_lock {
 public:
  void synchronize(); // NOLINT

  void lock_shared(); // NOLINT

  void unlock_shared(); // NOLINT

 private:
  std::atomic<size_t> epoch_;
  ThreadLocal<std::atomic<size_t>> timestamp_;

  void ReadLock();

  void ReadUnlock();
};

template <typename ValueType>
class PointerStorage {
  struct Cell {
    rcu_lock lock_;
    std::atomic<ValueType*> value_;

    Cell() : lock_(), value_(nullptr) {}

    ~Cell() {
      delete value_.load();
    }

    std::enable_if_t<
      std::is_copy_constructible_v<ValueType>
    > Set(const ValueType& value) {
      auto previous = value_.exchange(new ValueType(value));
    
      lock_.synchronize();
      delete previous;
    }
    
    ValueType Get() {
      std::shared_lock guard(lock_);

      return *(value_.load());
    }
  };

 public:
  PointerStorage(size_t size) : content_(size) {}

  void Set(size_t key, const ValueType& value) {
    content_[key].Set(value);
  }

  ValueType Get(size_t key) {
    return content_[key].Get();
  }

 private:
  std::vector<Cell> content_;
};

} // namespace factor
