/*
 * Inline header for various losses.
 */

#pragma once
#include <cmath>

namespace factor {

struct MSELoss {
  inline double GetLoss(double predict, double target) const {
    return (predict - target) * (predict - target);
  }

  inline double GetGradient(double predict, double target) const {
    return 2 * (predict - target);
  }
};

struct LogLoss {
  const double eps = 1e-12;

  inline double Sigmoid(double predict) const {
    return std::min(1 - eps, std::max(eps, 1 / (1 + exp(-predict))));
  }

  inline double GetLoss(double predict, double target) const {
    predict = Sigmoid(predict);
    if (target <= 1e-12) {
      predict = 1 - predict;
    }
    return -log(predict);
  }

  inline double GetGradient(double predict, double target) const {
    if (target <= 1e-12) {
      return -1 / (1 + exp(predict)) + 1;
    }
    return -1 / (1 + exp(predict));
  }
};

} // namespace factor
