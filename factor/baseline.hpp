#pragma once

#include <cmath>
#include <vector>
#include <deps/Eigen/Dense>


namespace factor {

struct BaselineModel {
  inline BaselineModel(size_t feature_count, size_t dimension) :
      dim_(dimension),
      bias_(0),
      weight_(feature_count, 0),
      vector_(feature_count) {
    for (auto&& vec : vector_) {
      vec = Eigen::VectorXd::Random(dim_) / std::sqrt(static_cast<double>(dim_));
    }
  }

  template <typename Feature>
  inline double Predict(const Feature& feature) const {
    double predict = bias_;

    Eigen::VectorXd sum = Eigen::VectorXd::Zero(dim_);
    for (auto&& [idx, val] : feature) {
      predict += weight_[idx] * val;
      sum += val * vector_[idx];
    }
    predict += 0.5 * sum.dot(sum);
    return predict;
  }

  template <typename Feature, typename Optimizer, typename Loss>
  inline void Fit(
      const Feature& feature,
      double target,
      const Loss& loss,
      const Optimizer& opt) {
    // Forward step
    double predict = bias_;

    Eigen::VectorXd sum = Eigen::VectorXd::Zero(dim_);
    for (auto&& [idx, val] : feature) {
      predict += weight_[idx] * val;
      sum += val * vector_[idx];
    }
    predict += 0.5 * sum.dot(sum);
    
    // Backward step
    double loss_gradient = loss.GetGradient(predict, target);
    for (auto&& [idx, val] : feature) {
      weight_[idx] = opt.Step(weight_[idx], val * loss_gradient);
      vector_[idx] = opt.template Step<Eigen::VectorXd>(
          vector_[idx],
          val * loss_gradient * sum
      );
    }
    bias_ = opt.Step(bias_, loss_gradient);
  }


  const size_t dim_;

  double bias_;
  std::vector<double> weight_;
  std::vector<Eigen::VectorXd> vector_;
};

} // namespace factor
