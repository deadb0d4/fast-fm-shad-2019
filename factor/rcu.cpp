#include <factor/rcu.hpp>

#include <limits>


namespace factor {

void rcu_lock::synchronize() {
  size_t start = epoch_.fetch_add(1);

  if (start > std::numeric_limits<size_t>::max() / 2) {
    throw std::runtime_error("Lock expired!");
  }
  for (auto it = timestamp_.begin(); it != timestamp_.end(); ++it) {
    while (*it < start)
      ;
  }
}

void rcu_lock::lock_shared() {
  ReadLock();
}

void rcu_lock::unlock_shared() {
  ReadUnlock();
}

void rcu_lock::ReadLock() {
  timestamp_->store(epoch_.fetch_add(1));
}

void rcu_lock::ReadUnlock() {
  timestamp_->store(std::numeric_limits<size_t>::max());
}

} // namespace factor
