# Factor library

Здесь лежат компоненты, содержащие логику самой (регрессионной) модели.
Два основных класса BaselineModel и ThreadSafeModel находятся в заголовочных
файлах `baseline.hpp` и `parallel.hpp`, представляют собой реализации известного
алгоритма [факторизационных машин](https://www.csie.ntu.edu.tw/~b97053/paper/Rendle2010FM.pdf).

Также, логика оптимизации и функции потерь разделены по заголовочным файлам
`loss.hpp` и `opt.hpp`.
