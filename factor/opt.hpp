/*
 * Inline header for various optimizers. Wonder if something except of simple
 * sgd is required...
 */

#pragma once


namespace factor {

struct BasicOptimizer {
  double learning_rate = 1e-4;
  double weight_decay  = 1.0;

  template <typename NumericType>
  NumericType Step(const NumericType& var, const NumericType& grad) const {
    return var - learning_rate * (weight_decay * var + grad);
  }
};

} // namespace factor
