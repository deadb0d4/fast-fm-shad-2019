#include <factor/lock_vector.hpp>


namespace factor {

void LockVector::Set(const Eigen::VectorXd& other) {
  std::unique_lock<std::mutex> guard(lock);
  
  value = other;
}

Eigen::VectorXd LockVector::Get() const {
  std::unique_lock<std::mutex> guard(lock);

  return value;
}

} // namespace factor
