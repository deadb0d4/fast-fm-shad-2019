#include "dataloader.hpp"

int main() {
    convert_fm_to_fmb("../data/netflix/val.fm");
    return 0;
    DataLoader data_loader("../data/netflix/val.fmb");
    std::vector<BatchGenerator> batch_generators = data_loader.get_batch_generators(2);
    Dataset batch = batch_generators[0].generate(5);
    std::cout << batch << std::endl;
    std::cout << batch_generators[1].generate(5) << std::endl;
    return 0;
}
