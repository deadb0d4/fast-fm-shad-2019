#include <thread>
#include <future>

#include <dataloader/utils.hpp>
#include <dataloader/dataloader.hpp>

#include <factor/loss.hpp>
#include <factor/opt.hpp>
#include <factor/parallel.hpp>

std::string loss_type;

template<typename Model, typename Loss, typename Opt>
void train_on_batch(Model *model, const Opt &opt, const Loss &loss,
                    BatchGenerator *batch_generator,
                    int batches, int batch_size) {

    while (batches--) {
        const Dataset dataset = batch_generator->generate(batch_size);
        for (size_t i = 0; i < dataset.size(); ++i) {
            model->Fit(dataset.X[i].features, dataset.y[i], loss, opt);
        }
    }
}

/*
 * if batches == -1, evaluate on the whole dataset
 * provided by batch_generator
 */
template<typename Model, typename Loss>
void eval_on_batch(Model *model, const Loss &loss,
                   BatchGenerator *batch_generator,
                   int batches, int batch_size, std::vector<double> *errors, int pos) {

    double sum_loss = 0;
    int cnt = 0;

    while (batches == -1 || batches--) {
        const Dataset dataset = batch_generator->generate(batch_size, batches == -1);
        if (dataset.size() == 0) {
            break;
        }
        for (size_t i = 0; i < dataset.size(); ++i) {
            sum_loss += loss.GetLoss(model->Predict(dataset.X[i].features), dataset.y[i]);
            ++cnt;
        }
    }
    errors->at(pos) = sum_loss / cnt;
}

template<typename Model, typename Loss>
double eval(Model *model, const Loss &loss, const DataLoader &val_loader,
            int number_of_threads = 4, int batch_size = 100) {

    std::vector<BatchGenerator> val_batch_generators = val_loader.get_batch_generators(number_of_threads);
    std::thread t[number_of_threads];
    std::vector<double> errors(number_of_threads);
    for (int i = 0; i < number_of_threads; ++i) {
        t[i] = std::thread(eval_on_batch<Model, Loss>, model, loss,
                           &val_batch_generators[i], -1, batch_size, &errors, i);
    }
    double error = 0;
    for (int i = 0; i < number_of_threads; ++i) {
        t[i].join();
        error += errors[i];
    }
    error /= number_of_threads;
    if (loss_type == "rmse") {
        error = sqrt(error);
    }
    return error;
}

template<typename Model, typename Loss, typename Opt>
void train(Model *model, const Opt &opt, const Loss &loss,
           const DataLoader &train_loader, const DataLoader &val_loader,
           int epochs = 100, int number_of_threads = 4, int batch_size = 1000,
           int train_batches_per_epoch = 10000, int val_batches_per_epoch = 100,
           const std::string &save_model_file_name = "-",
           const std::string &save_learning_curve_file_name = "-") {

    std::vector<BatchGenerator> train_batch_generators = train_loader.get_batch_generators(number_of_threads);
    std::vector<BatchGenerator> val_batch_generators = val_loader.get_batch_generators(number_of_threads);
    batch_size = std::max(1, batch_size / number_of_threads);
    std::thread t[number_of_threads];
    std::vector<double> losses;
    double elapsed_time = 0;
    double best_loss = 0;
    for (int epoch = 0; epoch < epochs; ++epoch) {
        Timer timer;
        timer.start();
        for (int i = 0; i < number_of_threads; ++i) {
             t[i] = std::thread(train_on_batch<Model, Loss, Opt>, model, opt, loss,
                                &train_batch_generators[i], train_batches_per_epoch, batch_size);
        }
        for (int i = 0; i < number_of_threads; ++i) {
            t[i].join();
        }

        std::vector<double> errors(number_of_threads);
        for (int i = 0; i < number_of_threads; ++i) {
            t[i] = std::thread(eval_on_batch<Model, Loss>, model, loss,
                               &val_batch_generators[i], val_batches_per_epoch, batch_size, &errors, i);
        }
        double error = 0;
        for (int i = 0; i < number_of_threads; ++i) {
            t[i].join();
            error += errors[i];
        }
        error /= number_of_threads;
        if (loss_type == "rmse") {
            error = sqrt(error);
        }
        losses.push_back(error);
        save_learning_curve(save_learning_curve_file_name, losses);
        if (epoch == 0 || best_loss > error) {
            save_model(save_model_file_name, *model);
            best_loss = error;
        }
        std::cout << std::fixed << std::setprecision(5);
        std::cout << "Epoch #" << epoch + 1 << " took " << timer.get_time() << "s";
        std::cout << ", val loss = " << error << std::endl;
        elapsed_time += timer.get_time();
    }
    std::cout << "Finished, elapsed time: " << elapsed_time << "s" << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Please, specify path to the config file" << std::endl;
        return 0;
    }
    const std::string config_file = argv[1];
    if (argc < 3) {
        std::cout << "Please, specify mode: train, eval, or convert. In case of \"convert\" mode, specify also a file for conversion" << std::endl;
        return 0;
    }
    const std::string mode = argv[2];
    if (mode != "train" && mode != "eval" && mode != "convert") {
        std::cout << "Please, specify mode: train, eval, or convert. In case of \"convert\" mode, specify also a file for conversion" << std::endl;
        return 0;
    }
    if (mode == "convert") {
        if (argc < 4) {
            std::cout << "Please, specify a file for conversion" << std::endl;
            return 0;
        }
        const std::string file = argv[3];
        convert_fm_to_fmb(file, true);
        return 0;
    }
    DataLoader train_data_loader(get_parameter<std::string>(config_file, "train_data"));
    DataLoader val_data_loader(get_parameter<std::string>(config_file, "val_data"));
    const int kFeaturesCount = std::max(train_data_loader.get_features_count(), val_data_loader.get_features_count());

    auto model = factor::ThreadSafeModel(kFeaturesCount, get_parameter<int>(config_file, "dim"));
    load_model(get_parameter<std::string>(config_file, "init_model"), &model);

    loss_type = get_parameter<std::string>(config_file, "loss");
    if (loss_type != "mse" && loss_type != "rmse" && loss_type != "logloss") {
        std::cout << "Incorrect loss parameter: " << loss_type << std::endl;
        std::cout << "Loss should be one of the following: mse, rmse, logloss" << std::endl;
        return 0;
    }

    if (mode == "train") {
        auto opt = factor::BasicOptimizer();
        opt.learning_rate = get_parameter<double>(config_file, "learning_rate");
        opt.weight_decay = get_parameter<double>(config_file, "weight_decay");

        if (loss_type == "logloss") {
            train(&model, opt, factor::LogLoss(), train_data_loader, val_data_loader,
                  get_parameter<int>(config_file, "epochs"),
                  get_parameter<int>(config_file, "number_of_threads"),
                  get_parameter<int>(config_file, "batch_size"),
                  get_parameter<int>(config_file, "train_batches_per_epoch"),
                  get_parameter<int>(config_file, "val_batches_per_epoch"),
                  get_parameter<std::string>(config_file, "save_model"),
                  get_parameter<std::string>(config_file, "save_learning_curve"));
        } else {
            train(&model, opt, factor::MSELoss(), train_data_loader, val_data_loader,
                  get_parameter<int>(config_file, "epochs"),
                  get_parameter<int>(config_file, "number_of_threads"),
                  get_parameter<int>(config_file, "batch_size"),
                  get_parameter<int>(config_file, "train_batches_per_epoch"),
                  get_parameter<int>(config_file, "val_batches_per_epoch"),
                  get_parameter<std::string>(config_file, "save_model"),
		  get_parameter<std::string>(config_file, "save_learning_curve"));
        }
    } else if (mode == "eval") {
        double res;
        if (loss_type == "logloss") {
            res = eval(&model, factor::LogLoss(), val_data_loader,
                       get_parameter<int>(config_file, "number_of_threads"),
                       get_parameter<int>(config_file, "batch_size"));
        } else {
            res = eval(&model, factor::MSELoss(), val_data_loader,
                       get_parameter<int>(config_file, "number_of_threads"),
                       get_parameter<int>(config_file, "batch_size"));
        }
        std::cout << std::fixed << std::setprecision(5);
        std::cout << "Val loss: " << res << std::endl;
    }
    return 0;
}
