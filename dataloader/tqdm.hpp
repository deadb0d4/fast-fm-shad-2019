#pragma once
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>

class Tqdm {
public:
    Tqdm(): it(0), len(-1), message("") {
    }

    Tqdm(long long len): it(0), len(len), message("") {
    }

    Tqdm(long long len, const std::string &message): it(0), len(len), message(message) {
    }

    void start() {
        std::cout << message << get_status();
    }

    void update() {
        std::string old_status = get_status();
        ++it;
        std::string new_status = get_status();
        if (old_status != new_status) {
            std::cout << '\r' << message << new_status;
        }
    }

    void update(long long to) {
        std::string old_status = get_status();
        it = to;
        std::string new_status = get_status();
        if (old_status != new_status) {
            std::cout << '\r' << message << new_status;
        }
    }

    void finish() {
        update(len);
        std::cout << std::endl;
    }

private:
    std::string get_status() const {
        std::stringstream s;
        s << std::fixed << std::setprecision(2) << 100.0 * it / len << '%';
        return s.str();
    }

    long long it, len;
    std::string message;
};
