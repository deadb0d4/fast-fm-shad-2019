#pragma once
#include <ctime>
#include <vector>
#include <fstream>
#include <cassert>
#include <factor/parallel.hpp>
#include <factor/baseline.hpp>
#include <dataloader/dataset.hpp>

class Timer {
public:
    Timer() {
    }

    void start() {
        start_time = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    }

    double get_time() const { // returns time in seconds
        return (std::chrono::high_resolution_clock::now().time_since_epoch().count() - start_time) / 1e9;
    }

private:
    long long start_time;
};

void save_learning_curve(const std::string &file_name, const std::vector<double> &losses) {
    if (file_name == "-") {
        return;
    }
    std::ofstream out(file_name);
    out << losses.size() << "\n";
    for (double loss : losses) {
        out << loss << " ";
    }
    out << "\n";
    out.close();
}

std::vector<double> load_learning_curve(const std::string &file_name) {
    if (file_name == "-") {
        return {};
    }
    std::ifstream in(file_name);
    int size;
    in >> size;
    std::vector<double> losses(size);
    for (int i = 0; i < size; ++i) {
        in >> losses[i];
    }
    return losses;
}

const int kBufSize = 1000000;

class BinaryWriter {
public:
    BinaryWriter(const std::string &file_name, int buf_size): out(file_name, std::ofstream::binary),
                                                              pos(0),
                                                              global_pos(0) {
        
	if (!out.is_open()) {
	    std::cout << "Cann't open file " + file_name << std::endl;
	    assert(false);
	} 
        while (buf_size % 4) {
            ++buf_size;
        }
        buffer.resize(buf_size);
    }

    void write_ll(long long x) {
        char *it = (char*) &x;
        for (int i = 0; i < 8; ++i) {
            write(*it);
            ++it;
        }
    }

    void write_int(int x) {
        char *it = (char*) &x;
        for (int i = 0; i < 4; ++i) {
            write(*it);
            ++it;
        }
    }

    void write_double(double x) {
        char *it = (char*) &x;
        for (int i = 0; i < 8; ++i) {
            write(*it);
            ++it;
        }
    }

    void write_float(float x) {
        char *it = (char*) &x;
        for (int i = 0; i < 4; ++i) {
            write(*it);
            ++it;
        }
    }

    void write_sample(const Sample &sample) {
        write_int(sample.features.size());
        for (const Feature &feature : sample.features) {
            write_int(feature.id);
            write_float(feature.value);
        }
    }

    void flush() {
        out.write(buffer.data(), pos);
        pos = 0;
    }

    long long tellg() const {
        return global_pos;
    }

    void seekg(long long to) {
        flush();
        out.seekp(to);
        global_pos = to;
    }

private:
    void write(char c) {
        if (pos == buffer.size()) {
            flush();
        }
        buffer[pos++] = c;
        ++global_pos;
    }

    int pos;
    long long global_pos;
    std::ofstream out;
    std::vector<char> buffer;
};

class BinaryReader {
public:
    BinaryReader(const std::string &file_name, int buf_size): in(file_name, std::ifstream::binary),
                                                              pos(0),
                                                              to_pos(0),
                                                              global_pos(0) {
        if (file_name != "") {
	    check_is_open(file_name);
	}
	while (buf_size % 4) {
            ++buf_size;
        }
        buffer.resize(buf_size);
    }

    void open(const std::string &file_name) {
        in.open(file_name, std::ifstream::binary);
	check_is_open(file_name);
    }

    long long read_ll() {
        long long x;
        char *it = (char*) &x;
        for (int i = 0; i < 8; ++i) {
            *it = read();
            ++it;
        }
        return x;
    }

    int read_int() {
        int x;
        char *it = (char*) &x;
        for (int i = 0; i < 4; ++i) {
            *it = read();
            ++it;
        }
        return x;
    }

    double read_double() {
        double x;
        char *it = (char*) &x;
        for (int i = 0; i < 8; ++i) {
            *it = read();
            ++it;
        }
        return x;
    }

    float read_float() {
        float x;
        char *it = (char*) &x;
        for (int i = 0; i < 4; ++i) {
            *it = read();
            ++it;
        }
        return x;
    }

    Sample read_sample() {
        Sample res;
        int features_count = read_int();
        for (int i = 0; i < features_count; ++i) {
            int feature_id = read_int();
            int feature_value = read_float();
            res.add({feature_id, feature_value});
        }
        return res;
    }

    void seekg(long long to) {
        to_pos = 0;
        pos = 0;
        in.clear();
        in.seekg(to);
        global_pos = to;
    }

    long long tellg() const {
        return global_pos;
    }

private:
    void check_is_open(const std::string &file_name) {
        if (!in.is_open()) {
	    std::cout << "Cann't open file " + file_name << std::endl;
	    assert(false);
	}
    }

    char read() {
        if (pos >= to_pos) {
            in.read(buffer.data(), buffer.size());
            if (in) {
                to_pos = buffer.size();
            } else {
                to_pos = in.gcount();
            }
            pos = 0;
        }
        ++global_pos;
        return buffer[pos++];
    }

    int pos, to_pos;
    long long global_pos;
    std::ifstream in;
    std::vector<char> buffer;
};

void save_model(const std::string &file_name, const factor::ThreadSafeModel &model) {
    if (file_name == "-") {
        return;
    }
    BinaryWriter binary_writer(file_name, kBufSize);
    binary_writer.write_int(model.weight_.size());
    binary_writer.write_double(model.bias_);
    for (size_t i = 0; i < model.weight_.size(); ++i) {
        binary_writer.write_double(model.weight_[i]);
    }
    for (size_t i = 0; i < model.vector_.size(); ++i) {
        const Eigen::VectorXd v = model.vector_[i].Get();
        for (size_t j = 0; j < model.dim_; ++j) {
            binary_writer.write_double(v[j]);
        }
    }
    binary_writer.flush();
}

void load_model(const std::string &file_name, factor::ThreadSafeModel *model) {
    if (file_name == "-") {
        return;
    }
    BinaryReader binary_reader(file_name, kBufSize);
    int features_count = binary_reader.read_int();
    assert(model->weight_.size() == features_count);
    assert(model->vector_.size() == features_count);
    model->bias_.store(binary_reader.read_double());
    for (size_t i = 0; i < features_count; ++i) {
        model->weight_[i].store(binary_reader.read_double());
    }
    Eigen::VectorXd v(model->dim_);
    for (size_t i = 0; i < features_count; ++i) {
        for (size_t j = 0; j < model->dim_; ++j) {
            v[j] = binary_reader.read_double();
        }
        model->vector_[i].Set(v);
    }
}

void save_model(const std::string &file_name, const factor::BaselineModel &model) {
    if (file_name == "-") {
        return;
    }
    BinaryWriter binary_writer(file_name, kBufSize);
    binary_writer.write_int(model.weight_.size());
    binary_writer.write_double(model.bias_);
    for (size_t i = 0; i < model.weight_.size(); ++i) {
        binary_writer.write_double(model.weight_[i]);
    }
    for (size_t i = 0; i < model.vector_.size(); ++i) {
        for (size_t j = 0; j < model.dim_; ++j) {
            binary_writer.write_double(model.vector_[i][j]);
        }
    }
    binary_writer.flush();
}

void load_model(const std::string &file_name, factor::BaselineModel *model) {
    if (file_name == "-") {
        return;
    }
    BinaryReader binary_reader(file_name, kBufSize);
    int features_count = binary_reader.read_int();
    assert(model->weight_.size() == features_count);
    assert(model->vector_.size() == features_count);
    model->bias_ = binary_reader.read_double();
    for (size_t i = 0; i < features_count; ++i) {
        model->weight_[i] = binary_reader.read_double();
    }
    for (size_t i = 0; i < features_count; ++i) {
        for (size_t j = 0; j < model->dim_; ++j) {
            model->vector_[i][j] = binary_reader.read_double();
        }
    }
}

template<typename T>
T get_parameter(const std::string &file_name, const std::string &param) {
    std::ifstream in(file_name);
    std::string key;
    T value;
    std::string comment;
    while (in >> key) {
        if (key != param) {
            std::getline(in, comment);
            continue;
        }
        assert(in >> value);
	std::cout << key << ": " << std::fixed << std::setprecision(5) << value << std::endl;
        return value;
    }
    std::cout << "Parameter \"" << param << "\" not found. Please, specify it in the config file" << std::endl;
    assert(false);
}
