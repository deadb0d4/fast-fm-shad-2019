#pragma once

#include <set>
#include <map>
#include <random>
#include <vector>
#include <cassert>
#include <utility>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <stdexcept>

struct Feature {
    size_t id;
    double value;

    Feature(size_t id, double value): id(id), value(value) {
    }
};

struct Sample {
    std::vector<Feature> features;

    void add(const Feature &feature) {
        features.push_back(feature);
    }

    std::vector<Feature>::iterator begin() {
	return features.begin();
    }

    std::vector<Feature>::iterator end() {
        return features.end();
    }

    std::vector<Feature>::const_iterator cbegin() const {
        return features.cbegin();
    }

    std::vector<Feature>::const_iterator cend() const {
        return features.cend();
    }
};

class Dataset {
public:
    int kFeatureCount;
    std::vector<Sample> X;
    std::vector<double> y;

    Dataset(int kFeatureCount): kFeatureCount(kFeatureCount) {
    }

    Dataset(const std::vector<Sample> &X, const std::vector<double> &y, int kFeatureCount):
        X(X),
        y(y),
        kFeatureCount(kFeatureCount) {
    }

    void clear() {
        X.clear();
        y.clear();
    }

    void add(const Sample &sample, double target) {
        X.push_back(sample);
        y.push_back(target);
    }

    void shuffle() {
        std::vector<int> p(size());
        for (size_t i = 0; i < size(); ++i) {
            p[i] = i;
        }
        std::random_shuffle(p.begin(), p.end());
        for (size_t i = 0; i < size(); ++i) {
            std::swap(X[i], X[p[i]]);
            std::swap(y[i], y[p[i]]);
        }
    }

    size_t size() const {
        return X.size();
    }

    std::pair<Dataset, Dataset> TrainTestSplit(double train_size_percents = 0.9, bool need_shuffle = true) {
        if (need_shuffle) {
            shuffle();
        }
        size_t train_size = train_size_percents * size();
        size_t test_size = size() - train_size;
        assert(train_size && test_size);
        Dataset train(kFeatureCount), test(kFeatureCount);
        for (size_t i = 0; i < train_size; ++i) {
            train.add(X[i], y[i]);
        }
        for (size_t i = train_size; i < size(); ++i) {
            test.add(X[i], y[i]);
        }
        return {train, test};
    }
};

std::ostream& operator << (std::ostream &os, const Sample &sample) {
    os << '[';
    bool is_first = true;
    for (const Feature &feature : sample.features) {
        if (!is_first) {
            os << ", ";
        }
        is_first = false;
        os << '(' << feature.id << ": ";
        os << std::setprecision(3 - (feature.value < 0)) << std::fixed << feature.value << ')';
    }
    os << ']';
    return os;
}

std::ostream& operator << (std::ostream &os, const Dataset &dataset) {
    os << "Features count: " << dataset.kFeatureCount << std::endl;
    const size_t first_rows = 4, last_rows = 2;
    bool was_dots = false;
    for (size_t i = 0; i < dataset.size(); ++i) {
        if (first_rows <= i && i + last_rows < dataset.size()) {
            if (!was_dots) {
                os << " ... " << dataset.size() - first_rows - last_rows << " samples ..." << std::endl;
                was_dots = true;
            }
            continue;
        }
        if (i == 0) {
            os << '[';
        } else {
            os << ' ';
        }
        os << '(' << dataset.X[i] << ", y: " << dataset.y[i] << ')';
        if (i + 1 < dataset.size()) {
            os << ',' << std::endl;
        } else {
            os << ']';
        }
    }
    return os;
}

Dataset GenerateRandomSynteticData(size_t size, const int kFeatureCount = 2) {
  Dataset dataset(kFeatureCount);

  std::mt19937 gen(1);
  std::uniform_real_distribution<double> dis(-1.0, 1.0);

  for (size_t idx = 0; idx < size; ++idx) {
    double target = 1.0;
    Sample sample;
    for (size_t feature = 0; feature < kFeatureCount; ++feature) {
      double val = dis(gen);
      target += val;
      sample.add({feature, val});
    }
    dataset.add(sample, target);
  }
  return dataset;
}

int StrToInt(const std::string &s) {
    int x = 0;
    for (char digit : s) {
        x = x * 10 + digit - '0';
    }
    return x;
}

std::string IntToStr(int number, int cnt_digits) {
    std::string res;
    for (int i = 0; i < cnt_digits; ++i) {
        res += number % 10 + '0';
        number /= 10;
    }
    std::reverse(res.begin(), res.end());
    return res;
}

Dataset LoadNetflixDataset(const std::string &path_to_dataset, int number_of_films = 100, bool show_progress = true) {
    number_of_films = std::min(number_of_films, 17770);
    if (show_progress) {
        std::cout << "Finding unique film ids and user ids: 0.00%";
    }
    std::set<int> unique_users, unique_films;
    std::set<std::string> unique_dates;
    for (int i = 1; i <= number_of_films; ++i) {
        std::string path_to_file = path_to_dataset + "mv_" + IntToStr(i, 7) + ".txt";
        std::ifstream file(path_to_file);
        if (!file.good()) {
            throw std::runtime_error("File not found: " + path_to_file + ". Please make sure that you've loaded dataset from https://archive.org/details/nf_prize_dataset.tar and specify correct path to it.");
        }
        int film_id;
        char delimiter;
        file >> film_id >> delimiter;
        int user_id, rating;
        std::string date;
        while (file >> user_id >> delimiter >> rating >> delimiter >> date) {
            unique_users.insert(user_id);
            unique_dates.insert(date);
        }
        unique_films.insert(film_id);
        if (show_progress) {
            std::cout << "\rFinding unique film ids and user ids: ";
            std::cout << std::setprecision(2) << std::fixed << 100.0 * i / number_of_films << "%";
        }
    }
    if (show_progress) {
        std::cout << std::endl << "Found ";
        std::cout << unique_films.size() << " unique films, ";
        std::cout << unique_users.size() << " unique users, " ;
        std::cout << unique_dates.size() << " unique dates" << std::endl;
    }
    std::vector<int> user_ids(unique_users.begin(), unique_users.end());
    std::vector<int> film_ids(unique_films.begin(), unique_films.end());
    std::vector<std::string> date_ids(unique_dates.begin(), unique_dates.end());

    if (show_progress) {
        std::cout << "Loading dataset: 0.00%";
    }
    std::vector<Sample> users_scores(user_ids.size());
    for (int i = 1; i <= number_of_films; ++i) {
        std::string path_to_file = path_to_dataset + "mv_" + IntToStr(i, 7) + ".txt";
        std::ifstream file(path_to_file);
        int film_id;
        char delimiter;
        file >> film_id >> delimiter;
        const int compressed_film_id = std::lower_bound(film_ids.begin(), film_ids.end(), film_id) - film_ids.begin();
        int user_id, rating;
        std::string date;
        while (file >> user_id >> delimiter >> rating >> delimiter >> date) {
            const int compressed_user_id = std::lower_bound(user_ids.begin(), user_ids.end(), user_id) - user_ids.begin();
            const int compressed_date_id = std::lower_bound(date_ids.begin(), date_ids.end(), date) - date_ids.begin();
            users_scores[compressed_user_id].add(Feature(compressed_film_id, rating));
        }
        if (show_progress) {
            std::cout << "\rLoading dataset: ";
            std::cout << std::setprecision(2) << std::fixed << 50.0 * i / number_of_films << "%";
        }
    }
    std::vector<Sample> X;
    std::vector<double> y;
    for (int i = 1; i <= number_of_films; ++i) {
        std::string path_to_file = path_to_dataset + "mv_" + IntToStr(i, 7) + ".txt";
        std::ifstream file(path_to_file);
        int film_id;
        char delimiter;
        file >> film_id >> delimiter;
        const int compressed_film_id = std::lower_bound(film_ids.begin(), film_ids.end(), film_id) - film_ids.begin();
        int user_id, rating;
        std::string date;
        while (file >> user_id >> delimiter >> rating >> delimiter >> date) {
            const int compressed_user_id = std::lower_bound(user_ids.begin(), user_ids.end(), user_id) - user_ids.begin();
            const int compressed_date_id = std::lower_bound(date_ids.begin(), date_ids.end(), date) - date_ids.begin();
            Sample sample = users_scores[compressed_user_id];
            sample.add(Feature(film_ids.size() + compressed_film_id, 1));
            // sample.add(Feature(2 * film_ids.size() + compressed_date_id, 1));
            X.push_back(sample);
            y.push_back(rating);
        }
        if (show_progress) {
            std::cout << "\rLoading dataset: ";
            std::cout << std::setprecision(2) << std::fixed << 50.0 + 50.0 * i / number_of_films << "%";
        }
    }
    if (show_progress) {
        std::cout << std::endl;
    }
    // return Dataset(X, y, 2 * film_ids.size() + date_ids.size());
    return Dataset(X, y, 2 * film_ids.size());
}

std::vector<std::string> Split(const std::string &s, const char delimiter = ',') {
    std::string last;
    std::vector<std::string> res;
    for (size_t i = 0; i < s.length(); ++i) {
        if (s[i] == delimiter) {
            res.push_back(last);
            last = "";
        } else {
            last += s[i];
        }
    }
    if (last != "") {
        res.push_back(last);
    }
    return res;
}

Dataset LoadAzureDataset(const std::string &path_to_dataset, int cnt = 100, bool show_progress = true) {
    std::string s;
    std::ifstream in(path_to_dataset);
    getline(in, s);
    std::vector<std::string> titles = Split(s);
    std::map<std::string, int> compressed[titles.size()];
    std::vector<int> first_number(titles.size()), start(titles.size());
    std::vector<size_t> good_features_id;
    size_t click_feature_id;
    for (size_t i = 0; i < titles.size(); ++i) {
        if (titles[i] != "id" && titles[i] != "device_ip" && titles[i] != "device_id" && titles[i] != "click") {
            good_features_id.push_back(i);
        }
        if (titles[i] == "click") {
            click_feature_id = i;
        }
    }

    if (show_progress) {
        std::cout << "Making one-hot encoding of all features: 0.00%";
    }
    for (size_t i = 1; i <= cnt; ++i) {
        getline(in, s);
        const auto v = Split(s);
        for (size_t j : good_features_id) {
            if (!compressed[j].count(v[j])) {
                compressed[j][v[j]] = first_number[j]++;
            }
        }
        if (show_progress && (i % 100 == 0 || i + 1 == cnt)) {
            std::cout << "\rMaking one-hot encoding of all features: ";
            std::cout << std::setprecision(2) << std::fixed << 100.0 * i / cnt << "%";
        }
    }
    if (show_progress) {
        std::cout << std::endl;
    }

    in.clear();
    in.seekg(0, in.beg);
    getline(in, s);
    int total_feature_count = std::accumulate(first_number.begin(), first_number.end(), 0);
    for (size_t i = 1; i < first_number.size(); ++i) {
        start[i] = first_number[i] + start[i - 1];
    }
    std::vector<Sample> X;
    std::vector<double> y;
    X.reserve(cnt);
    y.reserve(cnt);
    if (show_progress) {
        std::cout << "Loading dataset: 0.00%";
    }
    for (size_t i = 1; i <= cnt; ++i) {
        getline(in, s);
        const auto v = Split(s);
        Sample sample;
        for (size_t j : good_features_id) {
            sample.add(Feature(start[j] + compressed[j][v[j]], 1));
        }
        X.push_back(sample);
        y.push_back(StrToInt(v[click_feature_id]));
        if (show_progress && (i % 100 == 0 || i + 1 == cnt)) {
            std::cout << "\rLoading dataset: ";
            std::cout << std::setprecision(2) << std::fixed << 100.0 * i / cnt << "%";
        }
    }
    if (show_progress) {
        std::cout << std::endl;
    }

    return {X, y, total_feature_count};
}
