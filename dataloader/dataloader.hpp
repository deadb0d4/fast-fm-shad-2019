#pragma once
#include <dataloader/tqdm.hpp>
#include <dataloader/utils.hpp>
#include <dataloader/dataset.hpp>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>

const int kMaxBatches = 64;

/*
 * Converts .fm file into the binary format.
 * File .fm should have following structure:
 * target_1 feature_id_1_1:feature_val_1_1 ... feature_id_1_k1:feature_val_1_k1
 * target_2 feature_id_2_1:feature_val_2_1 ... feature_id_2_k2:feature_val_2_k2
 * ...
 * target_n feature_id_n_1:feature_val_n_1 ... feature_id_n_kn:feature_val_n_kn,
 * where target_i is float number representing target value if i-th object,
 * feature_id_i,j is nonnegative int number representing id of the corresponding feature,
 * feature_val_i,j is float number representing value of the corresponding feature,
 * k_i is number of feature i-th object have,
 * n is number of objects in datasets
 */
void convert_fm_to_fmb(std::string file_name, bool shuffle = true) {
    std::ifstream in(file_name);

    in.seekg(0, in.end);
    long long length = in.tellg();
    in.seekg(0, in.beg);
    Tqdm tqdm(length, "Converting to binary format: ");
    tqdm.start();

    BinaryWriter writer(file_name + "b", kBufSize);
    std::string s;
    writer.write_int(0); // imaginary features count
    for (int i = 0; i < kMaxBatches; ++i) {
        writer.write_ll(-1); // imaginary positions of batches' starts
    }
    writer.write_ll(-1); // imaginary end position
    Dataset dataset(0);
    int features = 0, written = 0, update_tqdm = 0;
    std::vector<long long> starts;
    while (std::getline(in, s)) {
        std::stringstream line(s);
        float target;
        line >> target;
        int feature_id;
        float feature_val;
        char delimiter;
        Sample sample;
        while (line >> feature_id >> delimiter >> feature_val) {
            sample.add({feature_id, feature_val});
            features = std::max(features, feature_id + 1);
            ++written;
        }
        dataset.add(sample, target);
        ++update_tqdm;
        if (update_tqdm == 1287) {
            tqdm.update(in.tellg());
            update_tqdm = 0;
        }
        if (written >= 1000000) {
            if (shuffle) {
                dataset.shuffle();
            }
            for (size_t i = 0; i < dataset.size(); ++i) {
                if (i % 100 == 0) {
                    starts.push_back(writer.tellg());
                }
                writer.write_sample(dataset.X[i]);
                writer.write_float(dataset.y[i]);
            }
            written = 0;
            dataset.clear();
        }
    }
    tqdm.finish();
    if (shuffle) {
        dataset.shuffle();
    }
    for (size_t i = 0; i < dataset.size(); ++i) {
        if (i % 100 == 0) {
            starts.push_back(writer.tellg());
        }
        writer.write_sample(dataset.X[i]);
        writer.write_float(dataset.y[i]);
    }
    long long total = writer.tellg();
    writer.flush();
    writer.seekg(0);
    writer.write_int(features);
    for (int i = 0; i < kMaxBatches; ++i) {
        writer.write_ll(starts[1LL * i * starts.size() / kMaxBatches]);
    }
    writer.write_ll(total);
    writer.flush();
}

class BatchGenerator {
public:
    BatchGenerator(): reader("", kBufSize), from(0), to(0), kFeaturesCount(0), initialized(false) {
    }

    void init(const std::string &file_name, long long from_, long long to_, int kFeaturesCount_) {
        initialized = true;
        from = from_;
        to = to_;
        kFeaturesCount = kFeaturesCount_;
        reader.open(file_name);
        reader.seekg(from);
    }

    Dataset generate(int cnt, bool stop = false) {
        Dataset dataset(kFeaturesCount);
        if (!initialized || from == to) {
            return dataset;
        }
        if (reader.tellg() == to) {
            if (stop) {
                return dataset;
            }
            reader.seekg(from);
        }
        while (cnt--) {
            Sample sample = reader.read_sample();
            float target = reader.read_float();
            dataset.add(sample, target);
            if (reader.tellg() == to) {
                if (stop) {
                    break;
                }
                reader.seekg(from);
            }
        }
        return dataset;
    }
private:
    int kFeaturesCount;
    bool initialized;
    BinaryReader reader;
    long long from, to;
};

class DataLoader {
public:
    DataLoader(const std::string &file_name): file_name(file_name) {
        BinaryReader reader(file_name, 10000);
        kFeaturesCount = reader.read_int();
        starts.resize(kMaxBatches + 1);
        for (size_t i = 0; i < starts.size(); ++i) {
            starts[i] = reader.read_ll();
        }
    }

    std::vector<BatchGenerator> get_batch_generators(int cnt) const {
        assert(1 <= cnt && cnt <= kMaxBatches);
        std::vector<BatchGenerator> batch_generators(cnt);
        for (int i = 0; i < cnt; ++i) {
            long long from = starts[i * (starts.size() - 1) / cnt];
            long long to = starts[(i + 1) * (starts.size() - 1) / cnt];
            batch_generators[i].init(file_name, from, to, kFeaturesCount);
        }
        return batch_generators;
    }

    int get_features_count() const {
        return kFeaturesCount;
    }

private:
    int kFeaturesCount;
    std::vector<long long> starts;
    std::string file_name;
};
