FROM ubuntu:18.04

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . ./
RUN chmod 777 script/install.sh
RUN chmod 777 script/test.sh
RUN ./script/install.sh
