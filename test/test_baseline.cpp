#include <gtest/gtest.h>

#include <test/testing.hpp>

#include <cmath>

#include <factor/baseline.hpp>
#include <factor/loss.hpp>
#include <factor/opt.hpp>


TEST(BaselineOnSyntetics, Sanity) {
  constexpr size_t kCount = 10000;
  constexpr size_t kDimension = 32;
  constexpr size_t kFeature = 500;
  constexpr size_t kTests = 100;
  
  SimpleSparseGenerator generator(kFeature);

  factor::MSELoss loss;
  factor::BasicOptimizer opt;

  // Aggressive:
  opt.learning_rate = 0.1;

  factor::BaselineModel baseline(kFeature, kDimension);

  auto mean_error = [&] () {
    double error = 0;
    SimpleSparseGenerator generator(kFeature);
    for (size_t _ = 0; _ < kTests; ++_) {
      const auto& [sample, target] = generator.GetTrainSample();
      error += loss.GetLoss(baseline.Predict(sample), target);
    }
    return error / kTests;
  };
  double start_error = mean_error();

  double final_error;
  for (size_t epoch = 0; epoch < kCount; ++epoch) {
    const auto& [sample, value] = generator.GetTrainSample();

    baseline.Fit(sample, value, loss, opt);

    if (epoch % 100 == 0) {
      final_error = mean_error();
      ASSERT_TRUE(start_error * 2 > final_error) << "MSE got worse" << final_error;
    }
  }
  final_error = mean_error();
  ASSERT_TRUE(start_error > final_error) << "MSE got worse" << final_error;
}
