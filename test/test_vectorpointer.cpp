#include <gtest/gtest.h>

#include <factor/parallel.hpp>
#include <test/testing.hpp>


TEST(vector_pointer, Sanity) {
  factor::VectorPointer vector;

  Eigen::VectorXd zeros = Eigen::VectorXd::Zero(3);
  vector.Set(zeros);

  zeros *= 2;
  ASSERT_EQ(vector.Get() * 2, zeros);
}

TEST(vector_pointer, Assignment) {
  factor::VectorPointer vector;

  vector.Set(Eigen::VectorXd::Random(3));
  ASSERT_EQ(vector.Get().size(), 3);

  vector.Set(Eigen::VectorXd::Random(3));
  ASSERT_EQ(vector.Get().size(), 3);
}

TEST(vector_pointer, SimpleContention) {
  factor::VectorPointer vector;

  vector.Set(Eigen::VectorXd::Random(64));

  Eigen::VectorXd sum = Eigen::VectorXd::Zero(64);
  auto routine = [&] (size_t count) {
    for (size_t idx = 0; idx < count; ++idx) {
      vector.Set(Eigen::VectorXd::Random(64));
    }
  };
  SimpleThreadPool pool;

  size_t count = 10000;
  pool.Submit(routine, count);
  for (size_t idx = 0; idx < count; ++idx) {
    sum += vector.Get();
  }
}
