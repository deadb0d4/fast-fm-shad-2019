#include <gtest/gtest.h>

#include <test/testing.hpp>

#include <chrono>
#include <shared_mutex>

#include <factor/rcu.hpp>


TEST(rcu, Sanity) {
  const size_t kReaders = 17;

  factor::rcu_lock lock;

  std::atomic<size_t> readers_completed{0};
  auto reader_routine = [&] () {
    std::shared_lock guard(lock);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    readers_completed.fetch_add(1);
  };

  SimpleThreadPool pool;
  for (size_t _ = 0; _ < kReaders; ++_) {
    pool.Submit(reader_routine);
  }
  // Lazy way to ensure that main thread will not preceed readers
  std::this_thread::sleep_for(std::chrono::milliseconds(50));

  // Wait for readers to finish
  lock.synchronize();

  // Check that main thread have been blocked by `lock`
  ASSERT_EQ(readers_completed.load(), kReaders);
}
