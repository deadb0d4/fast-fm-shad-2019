#include <vector>
#include <string>

#include <gtest/gtest.h>
#include <example/dataset.hpp>

TEST(DatasetSizeTest) {
  auto dataset = GenerateRandomSynteticData(100, 2);

  ASSERT_TRUE(dataset.size() == 100) << "Dataset size is incorrect: " << dataset.size();
}

TEST(TrainTestSplitTest) {
  auto dataset = GenerateRandomSynteticData(100, 2);
  auto p = dataset.TrainTestSplit();

  ASSERT_TRUE(p.first.size() == 90) << "Train set size is incorrect: " << p.first.size();
  ASSERT_TRUE(p.second.size() == 10) << "Test set size is incorrect: " << p.second.size();
}

TEST(SplitTest) {
  std::vector<std::string> v{"a", "ab,bc", "0,-1,a"};
  std::vector<std::string> res1{"a"};
  std::vector<std::string> res2{"ab", "bc"};
  std::vector<std::string> res3{"0", "-1", "a"};

  std::vector<vector<std::string>> res{res1, res2, res3};

  for (size_t i = 0; i < v.size(); ++i) {
    ASSERT_TRUE(Split(v[i]) == res[i]) << "Split for string " << v[i] << " is incorrect";
  }
}
