#include <gtest/gtest.h>

#include <test/testing.hpp>

#include <cmath>

#include <factor/parallel.hpp>
#include <factor/baseline.hpp>
#include <factor/loss.hpp>
#include <factor/opt.hpp>

TEST(ThreadSafeModelTest, IsBaselineWhenOneThread) {
  constexpr size_t kCount = 1000;
  constexpr size_t kDimension = 16;
  constexpr size_t kFeature = 50;
  constexpr double kTolerance = 1e-2;

  SimpleSparseGenerator generator(kFeature);

  factor::MSELoss loss;
  factor::BasicOptimizer opt;

  factor::BaselineModel baseline(kFeature, kDimension);
  factor::ThreadSafeModel thread_safe(kFeature, kDimension);

  thread_safe.bias_.store(baseline.bias_);
  for (size_t idx = 0; idx < kFeature; ++idx) {
    thread_safe.weight_[idx].store(baseline.weight_[idx]);
    thread_safe.vector_[idx].Set(baseline.vector_[idx]);
  }
  for (size_t _ = 0; _ < kCount; ++_) {
    const auto& [sample, value] = generator.GetTrainSample();

    baseline.Fit(sample, value, loss, opt);
    thread_safe.Fit(sample, value, loss, opt);
  }
  for (size_t count = 0; count < kCount; ++count) {
    const auto& sample = generator.GetTrainSample().first;

    double diff =
        std::fabs(baseline.Predict(sample) - thread_safe.Predict(sample));
    ASSERT_TRUE(diff < kTolerance) << "ThreadSafe != Baseline, diff: " << diff;
  }
}
