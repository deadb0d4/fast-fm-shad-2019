#include <cmath>

#include <gtest/gtest.h>

#include <factor/opt.hpp>


TEST(BasicOptimizerTest, SimpleOptimization) {
  const size_t kEpoch = 1000;

  auto gd = [] (double x) {
    return x;
  };
  double point = 5.0;

  factor::BasicOptimizer opt;
  
  // Speed it up
  opt.learning_rate = 1e-1;

  for (size_t idx = 0; idx < kEpoch; ++idx) {
    point = opt.Step(point, gd(point));
  }
  ASSERT_TRUE(std::fabs(point) < 1e-2) << "Value is too big: " << point;
}
