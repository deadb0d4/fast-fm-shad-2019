#pragma once

#include <map>
#include <thread>
#include <utility>
#include <random>


struct SimpleThreadPool {
  std::vector<std::thread> pool;

  template <class Function, class... Args>
  void Submit(Function&& f, Args&&... args) {
    pool.emplace_back(std::forward<Function>(f), std::forward<Args>(args)...);
  }

  void Join() {
    for (auto&& thread : pool) {
      thread.join();
    }
    pool.clear();
  }

  ~SimpleThreadPool() {
    Join();
  }
};

struct SimpleSparseGenerator {
  const size_t feature_count_;
  std::mt19937 gen;
  std::bernoulli_distribution coin;
  std::uniform_int_distribution<size_t> index;
  std::uniform_real_distribution<> value;

  SimpleSparseGenerator(size_t feature_count, double sparsity = 0.75)
      : feature_count_(feature_count),
        gen(std::random_device()()),
        coin(sparsity),
        index(0, feature_count - 1),
        value(-1, 1) {}

  auto GetTrainSample() {
    std::map<size_t, double> sample;
    double target = 0;
    do {
      sample[index(gen)] = value(gen);
      target += sample[index(gen)];
    } while (coin(gen));
    return std::make_pair(sample, target);
  }
};
