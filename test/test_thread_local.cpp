#include <gtest/gtest.h>

#include <thread>

#include <factor/thread_local.hpp>


TEST(ThreadLocalTest, Sanity) {
  factor::ThreadLocal<int> tl;
  *tl = 255;
  ASSERT_EQ(*tl, 255);
}

TEST(ThreadLocalTest, ItIsThreadLocal) {
  factor::ThreadLocal<int> tl;

  *tl = 1337;
  auto routine = [&tl] () {
    *tl = 255;
    ASSERT_EQ(*tl, 255);
  };
  std::thread other(routine);
  other.join();
  ASSERT_EQ(*tl, 1337);
}

TEST(ThreadLocalTest, AccessManyTimes) {
  factor::ThreadLocal<int> tl;

  const size_t count = 100000;
  for (size_t idx = 0; idx < count; ++idx) {
    *tl = idx;
  }
  ASSERT_EQ(*tl, count - 1);
}
