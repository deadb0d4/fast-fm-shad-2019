train_data ../data/netflix/train.fmb # path to file with train data
val_data ../data/netflix/val.fmb # path to file with validation/test data
init_model - # path to file for initial model initialization, "-" for no file
save_model ../data/model.bin # path to file for saving model while training, "-" for no file
save_learning_curve ../data/learning_curve # path to file for saving learning curve while training, "-" for no file
dim 4 # dimension of hidden vectors
learning_rate 0.0001 # learning rate for training
weight_decay 0.001 # weight decay for training
epochs 100 # number of epochs for training
number_of_threads 4 # number of threads, from 1 to 64
batch_size 1000
train_batches_per_epoch 10000 # integer number
val_batches_per_epoch 100 # integer number
loss rmse # should be on of the following: mse, rmse, logloss
