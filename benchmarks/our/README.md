**Запуск консольной утилиты**

Для запуска консольной утилиты необходимо выполнить исполняемый файл `./main`, которому необходимо передать следующие параметры командной строки:
1. `config` - путь к config-файлу, в которому записаны все необходимые настройки (подробнее про config-файл будет написано ниже)
2. `mode` - одно из трех значений `train` для обучения модели, `eval` для оценивания обученной модели, и `convert` для преобразований файла с данными формата .fm в бинарный формат. В случае, когда `mode = convert`, нужно также передать третий параметр командной строки - путь к файлу для конвертирования.

**Файл config**

В файле `config` нужно указать следующие параметры:
1. `train_data` - путь к бинарному файлу с данными для обучения (этот бинарный файл нужно предварительно получить с помощью конвертирования)
2. `val_data` - путь к бинарному файлу с данными для итоговой оценки
3. `init_model` - путь к файлу, из которого нужно инициализировать веса модели, или "-", если инициализировать веса не нужно
4. `save_model` - путь к файлу, в который будут сохраняться веса модели по мере обучения, или "-", если сохранять веса не нужно
5. `save_learning_curve` - путь к файлу, в который будет сохраняться learning_curve (значения `loss`-a на валидационных данных для каждой эпохи), или "-", если сохранять learning curve не нужно
6. `dim` - параметр модели, размер скрытых векторов
7. `learning_rate` - параметр оптимизатора, learning rate для обучения
8. `weight_decay` - параметр оптимизатора, weight decay для обучения
9. `epochs` - количество эпох для тренировки
10. `number_of_threads` - кол-во потоков, в которых будет выполняться обучение и оценивания модели
11. `batch_size` - размер batch size для обучения
12. `train_batches_per_epoch` - кол-во батчей, на которых будет обучаться модель за одну эпоху
13. `val_batches_per_epoch` - кол-во батчей, на которых будет считаться `val_loss` за одну эпоху
14. `loss` - одно из следующих значений: `mse`, `rmse`, `logloss`. Рекомендуется использовать `mse` и `rmse` для задач регрессии, `logloss` - для классификации

**Получение бинарных файлов с датасетом**

Как уже было написано выше, для получения бинарных файлов нужно выполнить команду `./main config_file convert path_to_fm_file`, где `path_to_fm_file` - путь к текстовому файлу формата .fm, в котором хранится исходный датасет.
.fm файл имеет следующую структуру:

`target_1` `feature_id_1_1`:`feature_val_1_1` ... `feature_id_1_k1`:`feature_val_1_k1`

`target_2` `feature_id_2_1`:`feature_val_2_1` ... `feature_id_2_k2`:`feature_val_2_k2`

...

`target_n` `feature_id_n_1`:`feature_val_n_1` ... `feature_id_n_kn`:`feature_val_n_kn`,

где `target_i` - это дробное число, представляющее целевую метку i-го объекта,
`feature_id_i,j` - неотрицательное целое число, представляющее id соответствующего признака,
`feature_val_i,j`- дробное число, представляющие значение соответствующего признака,
`k_i` - кол-во признаков у i-го объекта,
`n` - кол-во объектов в датасете.

Файлы такого .fm формата для датасетов netflix и avazu можно скачать с яндекс.диска (ссылка находится в readme предыдущей папки)

**Получение результатов для предобученных моделей**

Веса для предобученных моделей на датасетах netflix и avazu находятся в файлах `model_netflix.bin` и `model_avazu.bin` соответственно. Эти веса были получены при обучении модели с использованием следующих гиперпараметрах:

1. `dim` = 4
2. `learning_rate` = 0.001 для netflix dataset и `learning_rate` = 0.0001 для avazu dataset
3. `weight_decay` = 0
4. `epochs` = 500 для netflix dataset и `epochs` = 600 для avazu dataset
5. `batch_size` = 1000
6. `train_batches_per_epoch` = 10000
7. `val_batches_per_epoch` = 1000
8. `loss` = RMSE для netflix dataset и LogLoss для avazu dataset

Кривые обучения модели с описанными выше гиперпараметрами можно найти в папке results.

Для получения итогового результата необходимо в файле config в качестве параметра `init_model` указать соответствующий путь к файлу с весами модели и вызвать консольную утилиту - `./main config_file eval`.

Для netflix dataset итоговый результат должен получиться 0.89351 RMSE, а для avazu dataset - 0.39051 LogLoss.
