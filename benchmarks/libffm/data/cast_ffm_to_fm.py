from tqdm import tqdm

def cast_ffm_to_fm(input_path, output_path):
    with open(input_path, 'r') as r, open(output_path, 'w') as w:
        for line in tqdm(r):
            sequences = line.strip().split()
            for index in range(1, len(sequences)):
                sequences[index] = ':'.join(sequences[index].split(':')[1:])
            output_line = ' '.join(sequences)
            w.write(output_line + '\n')
    return
    

if __name__ == '__main__':
    # call the function, like
    cast_ffm_to_fm("train_to_val.ffm", "val.fm")
