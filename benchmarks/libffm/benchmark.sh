#!/bin/bash

# Model hyperparameters
l=$1    # reg lambda
k=$2    # num factors
t=$3    # num iter
r=$4    # learning rate
s=$5    # num threads 

data=$6 # avazu or netflix (binary classification or regression)
out=$7  # output file to save val predictions

echo "Fitting libffm model:"

if [ $data = "avazu" ]
then
  # Trainsforming text .ffm files to binary will take some time (~ 3 min! for avazu dataset)
  # because I have some issues (seg faults) while working with .bin files directly
  ./build/ffm-train -l $l -k $k -t $t -r $r -s $s -p ./data/"$data"/train_to_val.ffm ./data/"$data"/train_to_train.ffm model.model

  echo "Validating (log loss):"
  ./build/ffm-predict ./data/"$data"/train_to_val.ffm model.model "$out"
  
  # Saving some GiBs
  rm *.bin
fi

if [ $data = "netflix" ]
then
  ./build_regression/ffm-train -l $l -k $k -t $t -r $r -s $s -p ./data/"$data"/train_to_val.ffm ./data/"$data"/train_to_train.ffm model.model

  echo "Validating (rmse loss):"
  ./build_regression/ffm-predict ./data/"$data"/train_to_val.ffm model.model "$out"
fi
