# Docker container

## How to run it

Install docker and in a directory with ```Dockerfile``` and ```install.sh``` run
```
docker build -t [name of the image] .
```
Then run
```
docker run -v [local directory]:/home --name [container_name] --cap-add SYS_PTRACE -it [image_name]
```
Where -v flag binds your ```[local directory]``` to the ```/home``` in the container.
The option ```--cap-add SYS_PTRACE``` is needed for leak sanitizer.

Every time you need the container start it with:
```
docker start [container_name]
```
and then attach to it:
```
docker attach [container_name]
```

## Additional dependencies

Feel free to add into ```install.sh``` any additional dependecies for the project 
