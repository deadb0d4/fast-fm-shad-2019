#!/bin/sh

# exit when any command fails
set -e

echo "Testing [factor] project"

echo "Asan build"

mkdir build
cd build

cmake -DCMAKE_BUILD_TYPE=ASAN -G Ninja ../
ninja
ninja test

echo "Tsan build"

rm -rf *
cmake -DCMAKE_BUILD_TYPE=TSAN -G Ninja ../
ninja
ninja test

cd ../
rm -rf build
