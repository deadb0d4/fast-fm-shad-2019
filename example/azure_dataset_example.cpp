/*
 * Basic example of usage Dataset class.
 */

#include "dataset.hpp"

int main() {
    auto dataset = LoadAzureDataset("../data/avazu_ctr_prediction/train/train.csv", 1000000);
    std::cout << dataset << std::endl;
    return 0;
}
