/*
 * Basic example of usage Dataset class.
 */

#include <set>
#include <vector>
#include <utility>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <random>
#include <algorithm>


#include <factor/loss.hpp>
#include <factor/opt.hpp>
#include <factor/baseline.hpp>


using Sample = std::vector<std::pair<size_t, double>>;

std::string IntToStr(int number, int cnt_digits) {
    std::string res;
    for (int i = 0; i < cnt_digits; ++i) {
        res += number % 10 + '0';
        number /= 10;
    }
    std::reverse(res.begin(), res.end());
    return res;
}


class Dataset {
 public:
  int kFeatureCount;
  std::vector<Sample> X;
  std::vector<double> y;

  Dataset(int kFeatureCount) : kFeatureCount(kFeatureCount) {
  }

  Dataset(const std::vector<Sample>& X, const std::vector<double>& y,
          int kFeatureCount)
      : X(X), y(y), kFeatureCount(kFeatureCount) {
  }

  void add(const Sample& sample, double target) {
    X.push_back(sample);
    y.push_back(target);
  }

  void shuffle() {
    std::vector<int> p(size());
    for (size_t i = 0; i < size(); ++i) {
      p[i] = i;
    }
    std::random_shuffle(p.begin(), p.end());
    for (size_t i = 0; i < size(); ++i) {
      std::swap(X[i], X[p[i]]);
      std::swap(y[i], y[p[i]]);
    }
  }

  size_t size() const {
    return X.size();
  }

  std::pair<Dataset, Dataset> TrainTestSplit(double train_size_percents = 0.9,
                                             bool need_shuffle = true) {
    if (need_shuffle) {
      shuffle();
    }
    size_t train_size = train_size_percents * size();
    size_t test_size = size() - train_size;
    assert(train_size && test_size);
    Dataset train(kFeatureCount), test(kFeatureCount);
    for (size_t i = 0; i < train_size; ++i) {
      train.add(X[i], y[i]);
    }
    for (size_t i = train_size; i < size(); ++i) {
      test.add(X[i], y[i]);
    }
    return {train, test};
  }
};

Dataset LoadNetflixDataset(const std::string& path_to_dataset,
                           int number_of_films = 100) {
  number_of_films = std::min(number_of_films, 17770);
  std::cout << "Finding unique film ids and user ids: 0.00%";
  std::set<int> unique_users, unique_films;
  std::set<std::string> unique_dates;
  for (int i = 1; i <= number_of_films; ++i) {
    std::string path_to_file =
        path_to_dataset + "mv_" + IntToStr(i, 7) + ".txt";
    std::ifstream file(path_to_file);
    if (!file.good()) {
      throw std::runtime_error("Dataset not found: " + path_to_file);
    }
    int film_id;
    char delimiter;
    file >> film_id >> delimiter;
    int user_id, rating;
    std::string date;
    while (file >> user_id >> delimiter >> rating >> delimiter >> date) {
      unique_users.insert(user_id);
      unique_dates.insert(date);
    }
    unique_films.insert(film_id);
    std::cout << "\rFinding unique film ids and user ids: ";
    std::cout << std::setprecision(2) << std::fixed
              << 100.0 * i / number_of_films << "%";
  }
  std::cout << std::endl << "Found ";
  std::cout << unique_films.size() << " unique films, ";
  std::cout << unique_users.size() << " unique users, ";
  std::cout << unique_dates.size() << " unique dates" << std::endl;
  std::vector<int> user_ids(unique_users.begin(), unique_users.end());
  std::vector<int> film_ids(unique_films.begin(), unique_films.end());
  std::vector<std::string> date_ids(unique_dates.begin(), unique_dates.end());

  std::cout << "Loading dataset: 0.00%";
  std::vector<Sample> users_scores(user_ids.size());
  for (int i = 1; i <= number_of_films; ++i) {
    std::string path_to_file =
        path_to_dataset + "mv_" + IntToStr(i, 7) + ".txt";
    std::ifstream file(path_to_file);
    int film_id;
    char delimiter;
    file >> film_id >> delimiter;
    const int compressed_film_id =
        std::lower_bound(film_ids.begin(), film_ids.end(), film_id) -
        film_ids.begin();
    int user_id, rating;
    std::string date;
    while (file >> user_id >> delimiter >> rating >> delimiter >> date) {
      const int compressed_user_id =
          std::lower_bound(user_ids.begin(), user_ids.end(), user_id) -
          user_ids.begin();
      const int compressed_date_id =
          std::lower_bound(date_ids.begin(), date_ids.end(), date) -
          date_ids.begin();
      users_scores[compressed_user_id].push_back({compressed_film_id, rating});
    }
    std::cout << "\rLoading dataset: ";
    std::cout << std::setprecision(2) << std::fixed
              << 50.0 * i / number_of_films << "%";
  }
  std::vector<Sample> X;
  std::vector<double> y;
  for (int i = 1; i <= number_of_films; ++i) {
    std::string path_to_file =
        path_to_dataset + "mv_" + IntToStr(i, 7) + ".txt";
    std::ifstream file(path_to_file);
    int film_id;
    char delimiter;
    file >> film_id >> delimiter;
    const int compressed_film_id =
        std::lower_bound(film_ids.begin(), film_ids.end(), film_id) -
        film_ids.begin();
    int user_id, rating;
    std::string date;
    while (file >> user_id >> delimiter >> rating >> delimiter >> date) {
      const int compressed_user_id =
          std::lower_bound(user_ids.begin(), user_ids.end(), user_id) -
          user_ids.begin();
      const int compressed_date_id =
          std::lower_bound(date_ids.begin(), date_ids.end(), date) -
          date_ids.begin();
      Sample sample = users_scores[compressed_user_id];
      sample.push_back({film_ids.size() + compressed_film_id, 1});
      X.push_back(sample);
      y.push_back(rating);
    }
    std::cout << "\rLoading dataset: ";
    std::cout << std::setprecision(2) << std::fixed
              << 50.0 + 50.0 * i / number_of_films << "%";
  }
  std::cout << std::endl;
  return Dataset(X, y, 2 * film_ids.size());
}

void ExampleWithNetflixDataset(const std::string& path, size_t film_count) {
  auto dataset =
      LoadNetflixDataset(path, film_count);
  auto splitted_dataset = dataset.TrainTestSplit();
  auto train = splitted_dataset.first;
  auto test = splitted_dataset.second;

  auto opt = factor::BasicOptimizer();
  auto loss = factor::MSELoss();
  auto model = factor::BaselineModel(dataset.kFeatureCount, 4);

  for (size_t epoch = 0; epoch < 20; ++epoch) {
    train.shuffle();

    for (size_t i = 0; i < train.size(); ++i) {
      model.Fit(train.X[i], train.y[i], loss, opt);
    }

    double error = 0.0;
    for (size_t i = 0; i < test.size(); ++i) {
      error += loss.GetLoss(model.Predict(test.X[i]), test.y[i]);
    }
    std::cout << epoch << "\t" << error / test.size() << std::endl;
  }
}

int main() {
  ExampleWithNetflixDataset(
    "../tmp/training_set/",
    45
  );
  return 0;
}
